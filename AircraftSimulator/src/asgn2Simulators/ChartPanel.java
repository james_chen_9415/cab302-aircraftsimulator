package asgn2Simulators;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import asgn2Aircraft.Bookings;


public class ChartPanel {
	
	private static final String TITLE = "Aircraft Simulation";
	private JFreeChart chart;
	private List<Integer> queueList, refusedList;
	private List<Bookings> bookings;

	
	// Parameters used to draw first chart
	public ChartPanel(List<Bookings> bookings) {
		this.bookings = bookings;
		final TimeSeriesCollection dataset = createTimeSeriesData(); 
        chart = createChart(dataset);
	}
	
	
	// Parameters used to draw second chart
	public ChartPanel(List<Integer> queue,List<Integer> refuse) {
		this.queueList = queue;
        this.refusedList = refuse;
		final TimeSeriesCollection dataset2 = createTimeSeriesDataWithQueueAndRefused(); 
        chart = createChart(dataset2);
	}

	
	public org.jfree.chart.ChartPanel getChartPanel(){
		return new org.jfree.chart.ChartPanel(chart);
	}
	
	
	// Private helper to create axis of charts
	private TimeSeriesCollection createTimeSeriesData() {
		TimeSeriesCollection tsc = new TimeSeriesCollection(); 
		TimeSeries bookTotal = new TimeSeries("Total Bookings");
		TimeSeries econTotal = new TimeSeries("Economy");
		TimeSeries preTotal = new TimeSeries("Premium");
		TimeSeries busTotal = new TimeSeries("Business");
		TimeSeries FirstTotal = new TimeSeries("First");
		TimeSeries emptyTotal = new TimeSeries("Empty");
		
		//Base time, data set up - the calendar is needed for the time points
		Calendar cal = GregorianCalendar.getInstance();
		//Random rng = new Random(250); 
		
		int economy = 0;
		int business = 0; 
		int premium = 0;
		int first = 0;
		int empty = 0;
		
		
		//Hack loop to make it interesting. Grows for half of it, then declines
		for (int i=Constants.FIRST_FLIGHT; i<=Constants.DURATION; i++) {
			//These lines are important 
			cal.set(2016,0,i,24,0);
	        Date timePoint = cal.getTime();

	       
	        if(bookings.get(i) != null){
		        first = bookings.get(i).getNumFirst();
		        business = bookings.get(i).getNumBusiness();
		        premium = bookings.get(i).getNumPremium();
		        economy = bookings.get(i).getNumEconomy();
		        empty = bookings.get(i).getAvailable();
	        }else{
	        	first = 0;
	        	business = 0;
	        	premium = 0;
	        	economy = 0;
	        }
	
			
	        //This is important - steal it shamelessly 
	        busTotal.add(new Day(timePoint),business);
	        preTotal.add(new Day(timePoint),premium);
	        FirstTotal.add(new Day(timePoint),first);
			econTotal.add(new Day(timePoint),economy);
			emptyTotal.add(new Day(timePoint),empty);
			bookTotal.add(new Day(timePoint),economy+business+first+premium);
		}
		
		//Collection
		tsc.addSeries(econTotal);
		tsc.addSeries(preTotal);
		tsc.addSeries(busTotal);
		tsc.addSeries(FirstTotal);
		tsc.addSeries(emptyTotal);
		tsc.addSeries(bookTotal);
		return tsc; 
	}
	
	
	 /**
     * Private method creates the dataset. Lots of hack code in the 
     * middle, but you should use the labelled code below  
	 * @return collection of time series for the plot 
	 */
	private TimeSeriesCollection createTimeSeriesDataWithQueueAndRefused() {
		TimeSeriesCollection tsc = new TimeSeriesCollection(); 
		TimeSeries queueTotal = new TimeSeries("Queue");
		TimeSeries refusedTotal = new TimeSeries("Refused");
		
		//Base time, data set up - the calendar is needed for the time points
		Calendar cal = GregorianCalendar.getInstance();
		//Random rng = new Random(250); 
		
		int queue = 0;
		int refuse = 0; 
		
		
		//Hack loop to make it interesting. Grows for half of it, then declines
		for (int i=21; i<=18*7; i++) {
			//These lines are important 
			cal.set(2016,0,i,6,0);
	        Date timePoint = cal.getTime();
	       
	        queue = queueList.get(i);
	        refuse = refusedList.get(i);
	        
	        //This is important - steal it shamelessly 
	        queueTotal.add(new Day(timePoint),queue);
	        refusedTotal.add(new Day(timePoint),refuse);
		}
		
		//Collection
		tsc.addSeries(queueTotal);
		tsc.addSeries(refusedTotal);
		return tsc; 
	}
	
    /**
     * Helper method to deliver the Chart - currently uses default colours and auto range 
     * @param dataset TimeSeriesCollection for plotting 
     * @returns chart to be added to panel 
     */
    private JFreeChart createChart(final XYDataset dataset) {
        final JFreeChart result = ChartFactory.createTimeSeriesChart(
            TITLE, "Days", "Passengers", dataset, true, true, false);
        final XYPlot plot = result.getXYPlot();
        ValueAxis domain = plot.getDomainAxis();
        domain.setAutoRange(true);
        ValueAxis range = plot.getRangeAxis();
        range.setAutoRange(true);
        return result;
    }
	

}
