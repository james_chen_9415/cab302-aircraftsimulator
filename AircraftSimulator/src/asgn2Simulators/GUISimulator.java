/**
 * 
 * This file is part of the AircraftSimulator Project, written as 
 * part of the assessment for CAB302, semester 1, 2016. 
 * 
 */
package asgn2Simulators;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import asgn2Aircraft.AircraftException;
import asgn2Aircraft.Bookings;
import asgn2Passengers.PassengerException;


/**
 * @author hogan
 *
 */
@SuppressWarnings("serial")
public class GUISimulator extends JFrame implements Runnable {

	public static final int WIDTH = 950;
	public static final int HEIGHT = 700;
	private Log log;
	private Simulator sim;

	// Pop Up Window
	private JPanel pnlMessage;
	// Major Panels
	private JPanel pnlMenu;
	private JPanel pnlBtn;
	private JPanel pnlDisplay;
	private JScrollPane scrollPane;
	private asgn2Simulators.ChartPanel chartPanel;

	// output is contained in scrollPane
	private JTextArea output;

	// Buttons
	private JButton btnRunSim, btnShowChart;

	// TextFields
	private JTextField seedTxtField;
	private JTextField dailyMeanTxtField;
	private JTextField queueSizeTxtField;
	private JTextField cancellationTxtField;
	private JTextField firstTxtField;
	private JTextField businessTxtField;
	private JTextField premiumTxtField;
	private JTextField economyTxtField;
	// Labels
	private JLabel simulationLabel, fareClassLabel;
	private JLabel seedLabel, meanLabel, queueSizeLabel, cancellationLabel;
	// Fare classes labels
	private JLabel firstLabel, businessLabel, premiumLabel, economyLabel;

	// Data to create charts
	private List<Integer> queueList, refusedList;
	private List<Bookings> bookings;
	
	private boolean getAttributeFromSimulationRunner = false;
	private String[] nineAttribute;

	
	/**
	 * @param arg0
	 * @throws HeadlessException
	 */
	public GUISimulator(String arg0) throws HeadlessException {
		super(arg0);
	}

	
	/**
	 * @param arg0
	 * @throws HeadlessException
	 */
	public GUISimulator(String arg0, String[] arg1) throws HeadlessException {
		super(arg0);
		this.nineAttribute = arg1;
		this.getAttributeFromSimulationRunner = true;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		createGUI();
	}

	
	private void createGUI() {

		setSize(WIDTH, HEIGHT);
		setTitle("Simulator");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());

		// major panels
		pnlMenu = createPanel(Color.LIGHT_GRAY);
		pnlBtn = createPanel(Color.LIGHT_GRAY);
		pnlDisplay = createPanel(Color.WHITE);

		// Text fields on left panel
		if (getAttributeFromSimulationRunner) {
			seedTxtField = createTextField(nineAttribute[0]);
			queueSizeTxtField = createTextField(nineAttribute[1]);
			dailyMeanTxtField = createTextField(nineAttribute[2]);

			firstTxtField = createTextField(nineAttribute[4]);
			businessTxtField = createTextField(nineAttribute[5]);
			premiumTxtField = createTextField(nineAttribute[6]);
			economyTxtField = createTextField(nineAttribute[7]);
			cancellationTxtField = createTextField(nineAttribute[8]);
		} else {
			seedTxtField = createTextField(Integer.toString(Constants.DEFAULT_SEED));
			queueSizeTxtField = createTextField(Integer.toString(Constants.DEFAULT_MAX_QUEUE_SIZE));
			dailyMeanTxtField = createTextField(Double.toString(Constants.DEFAULT_DAILY_BOOKING_MEAN));
			
			firstTxtField = createTextField(Double.toString(Constants.DEFAULT_FIRST_PROB));
			businessTxtField = createTextField(Double.toString(Constants.DEFAULT_BUSINESS_PROB));
			premiumTxtField = createTextField(Double.toString(Constants.DEFAULT_PREMIUM_PROB));
			economyTxtField = createTextField(Double.toString(Constants.DEFAULT_ECONOMY_PROB));
			cancellationTxtField = createTextField(Double.toString(Constants.DEFAULT_CANCELLATION_PROB));
		}

		// Big Labels
		simulationLabel = createLabel("Simulation");
		fareClassLabel = createLabel("Fare Classes");
		simulationLabel.setFont(new Font(simulationLabel.getName(), Font.PLAIN, 25));
		fareClassLabel.setFont(new Font(fareClassLabel.getName(), Font.PLAIN, 25));

		// Input Label
		seedLabel = createLabel("RNG Seed: ");
		meanLabel = createLabel("Daily Mean: ");
		queueSizeLabel = createLabel("Queue size: ");
		cancellationLabel = createLabel("Cancellation: ");
		// label fare classes
		firstLabel = createLabel("First");
		businessLabel = createLabel("Business");
		premiumLabel = createLabel("Premium");
		economyLabel = createLabel("Economy");

		// buttons on bottom panel
		btnRunSim = createButton("Run Simulation");
		btnShowChart = createButton("Show Queued&Refused");
		btnShowChart.setEnabled(false);

		// textarea on the top of pnlDisplay. This is contained in scrollPane
		output = new JTextArea(null, "", 0, 100);
		output.setEditable(false);
		output.setLineWrap(true);

		// scrollPane
		scrollPane = new JScrollPane(output);
		scrollPane.setPreferredSize(new Dimension(750, 180));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

		// Adding panels West, South and Center to JFrame
		this.getContentPane().add(pnlMenu, BorderLayout.WEST);
		this.getContentPane().add(pnlBtn, BorderLayout.SOUTH);
		this.getContentPane().add(pnlDisplay, BorderLayout.CENTER);
		layoutPanelWest();
		layoutPanelSouth();

		repaint();
		this.setVisible(true);

	}

	
	private JButton createButton(String str) {
		JButton jb = new JButton(str);
		jb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Object src = e.getSource();
				if (src == btnRunSim) {
					
					if (validateInput()) {

						double sdBookings;
						if (getAttributeFromSimulationRunner) {
							sdBookings = Double.parseDouble(nineAttribute[3]);
						} else {
							sdBookings = Constants.DEFAULT_DAILY_BOOKING_SD;
						}
						int seed = Integer.parseInt(seedTxtField.getText());
						int maxQueueSize = Integer.parseInt(queueSizeTxtField.getText());
						double meanBookings = Double.parseDouble(dailyMeanTxtField.getText());

						double firstProb = Double.parseDouble(firstTxtField.getText());
						double businessProb = Double.parseDouble(businessTxtField.getText());
						double premiumProb = Double.parseDouble(premiumTxtField.getText());
						double economyProb = Double.parseDouble(economyTxtField.getText());
						double cancelProb = Double.parseDouble(cancellationTxtField.getText());
						try {
							runSimulation(seed, maxQueueSize, meanBookings, sdBookings, firstProb, 
									businessProb, premiumProb, economyProb, cancelProb);
						} catch (AircraftException | PassengerException | SimulationException | IOException e1) {
							e1.printStackTrace();
						}
//						layoutPanelDisplay();
//						output.setRows(output.getLineCount());
						
						chartPanel = new asgn2Simulators.ChartPanel(bookings);

						pnlDisplay.removeAll();
						
						layoutPanelDisplay(chartPanel);
						//pnlDisplay.add(chartPanel.getChartPanel());

						repaint();
						setVisible(true);
						btnShowChart.setEnabled(true);
						txtFieldEnabled();

					}
				}

				if (src == btnShowChart) {
					pnlDisplay.removeAll();
					chartPanel = new asgn2Simulators.ChartPanel(queueList, refusedList);
					layoutPanelDisplay(chartPanel);
					//pnlDisplay.add(chartPanel.getChartPanel());
					repaint();
					setVisible(true);
					btnShowChart.setEnabled(false);
				}
			}
		});
		return jb;
	}

	
	private void txtFieldEnabled() {
		seedTxtField.setEnabled(true);
		dailyMeanTxtField.setEnabled(true);
		queueSizeTxtField.setEnabled(true);
		cancellationTxtField.setEnabled(true);
		firstTxtField.setEnabled(true);
		businessTxtField.setEnabled(true);
		premiumTxtField.setEnabled(true);
		economyTxtField.setEnabled(true);
	}

	
	// Helper method to validate input 
	private boolean validateInput() {
		boolean flag = true;
		// checking if fare classes is not empty
		if (firstTxtField.getText().isEmpty() || businessTxtField.getText().isEmpty() 
				|| premiumTxtField.getText().isEmpty() || economyTxtField.getText().isEmpty() 
				|| seedTxtField.getText().isEmpty() || dailyMeanTxtField.getText().isEmpty() 
				|| queueSizeTxtField.getText().isEmpty() || cancellationTxtField.getText().isEmpty()) {
			flag = false;
		}
		try {
			float first = Float.parseFloat(firstTxtField.getText());
			float business = Float.parseFloat(businessTxtField.getText());
			float premium = Float.parseFloat(premiumTxtField.getText());
			float economy = Float.parseFloat(economyTxtField.getText());
			float seed = Float.parseFloat(seedTxtField.getText());
			float dailyMean = Float.parseFloat(dailyMeanTxtField.getText());
			float queueSize = Float.parseFloat(queueSizeTxtField.getText());
			float cancellation = Float.parseFloat(cancellationTxtField.getText());

			Boolean posNumber = true;

			if (first < 0 || business < 0 || premium < 0 || economy < 0 || seed < 0 || dailyMean < 0 || queueSize < 0 || cancellation < 0) {
				posNumber = false;
			}
			// if class fares don't sum 1, then flag is false
			flag = first + business + premium + economy == 1.0 && flag;

			if (!flag) {
				JOptionPane.showMessageDialog(pnlMessage, "The total sum of class fares must be 1 with positive numbers.", "Invalid input", JOptionPane.ERROR_MESSAGE);
			}
			if (!posNumber) {
				JOptionPane.showMessageDialog(pnlMessage, "Enter only positive numbers.", "Invalid input", JOptionPane.ERROR_MESSAGE);
			}

			return flag && posNumber;
			// if the textfield doesnt contain a number, throw exception and return false
		} catch (NumberFormatException n) {
			JOptionPane.showMessageDialog(pnlMessage, "This is not a number.", "Invalid input", JOptionPane.ERROR_MESSAGE);
			return false;
		}

	}

	
	private JPanel createPanel(Color c) {
		JPanel jp = new JPanel();
		jp.setBackground(c);
		return jp;
	}

	
	private JTextField createTextField(String str) {
		JTextField textField = new JTextField(str, 5);
		textField.setEnabled(false);
		return textField;
	}

	
	private JLabel createLabel(String str) {
		JLabel jLabel = new JLabel(str);
		return jLabel;
	}

	
	private void layoutPanelWest() {

		GridBagLayout layout = new GridBagLayout();
		pnlMenu.setLayout(layout);
		pnlMenu.setPreferredSize(new Dimension(170, HEIGHT));

		// add components to grid
		GridBagConstraints constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 0;
		constraints.weighty = 100;

		addToPanel(pnlMenu, simulationLabel, constraints, 0, 0, 3, 1);
		addToPanel(pnlMenu, seedLabel, constraints, 0, 2, 1, 1);
		addToPanel(pnlMenu, seedTxtField, constraints, 2, 2, 1, 1);
		addToPanel(pnlMenu, meanLabel, constraints, 0, 4, 1, 1);
		addToPanel(pnlMenu, dailyMeanTxtField, constraints, 2, 4, 1, 1);
		addToPanel(pnlMenu, queueSizeLabel, constraints, 0, 6, 1, 1);
		addToPanel(pnlMenu, queueSizeTxtField, constraints, 2, 6, 1, 1);
		addToPanel(pnlMenu, cancellationLabel, constraints, 0, 8, 1, 1);
		addToPanel(pnlMenu, cancellationTxtField, constraints, 2, 8, 1, 1);
		// Input Fare class
		addToPanel(pnlMenu, fareClassLabel, constraints, 0, 10, 3, 1);
		addToPanel(pnlMenu, firstLabel, constraints, 0, 12, 1, 1);
		addToPanel(pnlMenu, firstTxtField, constraints, 2, 12, 1, 1);
		addToPanel(pnlMenu, businessLabel, constraints, 0, 14, 1, 1);
		addToPanel(pnlMenu, businessTxtField, constraints, 2, 14, 1, 1);
		addToPanel(pnlMenu, premiumLabel, constraints, 0, 16, 1, 1);
		addToPanel(pnlMenu, premiumTxtField, constraints, 2, 16, 1, 1);
		addToPanel(pnlMenu, economyLabel, constraints, 0, 18, 1, 1);
		addToPanel(pnlMenu, economyTxtField, constraints, 2, 18, 1, 1);

	}

	
	private void layoutPanelDisplay(ChartPanel chartPanel) {
		FlowLayout layout = new FlowLayout(FlowLayout.CENTER, 80,5);
		pnlDisplay.setLayout(layout);
		pnlDisplay.add(scrollPane);
		output.setRows(output.getLineCount());
		pnlDisplay.add(chartPanel.getChartPanel());
	}

	
	private void layoutPanelSouth() {
		FlowLayout layout = new FlowLayout(FlowLayout.CENTER, 80, 5);
		pnlBtn.setLayout(layout);
		pnlBtn.setPreferredSize(new Dimension(WIDTH, 35));

		// add components to grid
		GridBagConstraints constraints = new GridBagConstraints();

		// Defaults
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 1;
		constraints.weighty = 0;

		addToPanel(pnlBtn, btnRunSim, constraints, 0, 0, 2, 1);
		addToPanel(pnlBtn, btnShowChart, constraints, 2, 0, 2, 1);

	}

	
	/**
	 * 
	 * A convenience method to add a component to given grid bag layout
	 * locations. Code due to Cay Horstmann
	 *
	 * @param c
	 *            the component to add
	 * @param constraints
	 *            the grid bag constraints to use
	 * @param x
	 *            the x grid position
	 * @param y
	 *            the y grid position
	 * @param w
	 *            the grid width
	 * @param h
	 *            the grid height
	 */
	private void addToPanel(JPanel jp, Component c, GridBagConstraints constraints, int x, int y, int w, int h) {
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridwidth = w;
		constraints.gridheight = h;
		jp.add(c, constraints);
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		SwingUtilities.invokeLater(new GUISimulator("Aircraft Simulator"));
	}

	
	/**
	 * Method to run the simulation from start to finish. Exceptions are
	 * propagated upwards as necessary
	 * 
	 * @throws AircraftException
	 *             See methods from {@link asgn2Simulators.Simulator}
	 * @throws PassengerException
	 *             See methods from {@link asgn2Simulators.Simulator}
	 * @throws SimulationException
	 *             See methods from {@link asgn2Simulators.Simulator}
	 * @throws IOException
	 *             on logging failures See methods from
	 *             {@link asgn2Simulators.Log}
	 * 
	 */
	public void runSimulation(int seed, int maxQueueSize, double meanDailyBookings, double sdDailyBookings, double firstProb, double businessProb, double premiumProb,
			double economyProb, double cancelProb) throws AircraftException, PassengerException, SimulationException, IOException {

		queueList = new ArrayList<Integer>();
		refusedList = new ArrayList<Integer>();
		bookings = new ArrayList<Bookings>();

		output.setText("");

		sim = new Simulator(seed, maxQueueSize, meanDailyBookings, sdDailyBookings, firstProb, 
				businessProb, premiumProb, economyProb, cancelProb);
		log = new Log();

		this.sim.createSchedule();
		this.log.initialEntry(this.sim);
		this.initialEntry(this.sim);

		// Main simulation loop
		for (int time = 0; time <= Constants.DURATION; time++) {
			this.sim.resetStatus(time);
			this.sim.rebookCancelledPassengers(time);
			this.sim.generateAndHandleBookings(time);
			this.sim.processNewCancellations(time);
			if (time >= Constants.FIRST_FLIGHT) {
				this.sim.processUpgrades(time);
				this.sim.processQueue(time);
				this.sim.flyPassengers(time);
				this.sim.updateTotalCounts(time);
				this.log.logFlightEntries(time, sim);
			} else {
				this.sim.processQueue(time);
			}
			// Log progress
			this.log.logQREntries(time, sim);
			this.log.logEntry(time, this.sim);
			logEntry(time, sim);

			queueList.add(sim.numInQueue());
			refusedList.add(sim.numRefused());
			// getting data to bookings
			if (time >= Constants.FIRST_FLIGHT)
				bookings.add(sim.getFlightStatus(time));
			else
				bookings.add(null);

		}
		this.sim.finaliseQueuedAndCancelledPassengers(Constants.DURATION);
		this.log.logQREntries(Constants.DURATION, sim);
		this.log.finalise(this.sim);
		finalise(sim);
	}

	
	// helper method called by runSimulation()
	private void finalise(Simulator sim) throws IOException {
		String time = getLogTime();
		output.setText(output.getText() + "\n" + time + ": End of Simulation\n");
		output.setText(output.getText() + sim.finalState());
	}

	
	// Helper method called by runSimulation()
	private void initialEntry(Simulator sim) throws SimulationException {
		output.setText(output.getText() + getLogTime() + ": Start of Simulation\n");
		output.setText(output.getText() + sim.toString() + "\n");
		String capacities = sim.getFlights(Constants.FIRST_FLIGHT).initialState();
		output.setText(output.getText() + capacities);
	}

	
	// Helper method called by runSimulation()
	private void logEntry(int time, Simulator sim) throws SimulationException {
		boolean flying = (time >= Constants.FIRST_FLIGHT);
		output.setText(output.getText() + sim.getSummary(time, flying));
	}

	
	/**
	 * Helper returning Log Time format for filename
	 * 
	 * @return filename String yyyyMMdd_HHmmss
	 */
	private String getLogTime() {
		String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		return timeLog;
	}

}