/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import javax.management.loading.PrivateClassLoader;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;
import javax.swing.plaf.basic.BasicSliderUI.ActionScroller;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.sun.xml.internal.ws.encoding.soap.SOAP12Constants;

import asgn2Aircraft.AircraftException;
import asgn2Aircraft.Bookings;
import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;
import asgn2Simulators.Log;
import asgn2Aircraft.A380;
import asgn2Aircraft.Aircraft;

/**
 * @author Renzo & James
 *
 */
public class A380Tests {

	private A380 airCraft;
	private Economy pE;
	private Premium pP;
	private Business pB;
	private First pF;
	/** Only for reference: These are the default values for the aircraft capacities of each class
	*   private final int FIRST = 14;
	*   private final int BUSINESS = 64;
	*   private final int PREMIUM = 35;
	*   private final int ECONOMY = 371;
	*   Current number of passenger of a particular class initialize at ZERO
	*/
	
	
	/**
	 * Set Up method to implement tests
	 */
	@Before
	public void noTestSetUp() throws Exception {
		airCraft = new A380("A380", 20);
		pE = new Economy(10, 20); //bookingtime, departuretime
		pP = new Premium(10, 20);
		pB = new Business(10, 20);
		pF = new First(10, 20);
	}
	
	
	// TESTING CONSTRUCTOR WITH TWO PARAMETERS
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int)}.
	 * @throws AircraftException 
	 * Empty string is allowed. Does not throw an exception
	 */
	public void constructorStringTest() throws AircraftException {
		airCraft = new A380("", 1);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when FlightCode is null
	 */
	@Test(expected = AircraftException.class)
	public void constructorStringNullTest() throws AircraftException {
		airCraft = new A380(null, 1);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when departure time is 0
	 */
	@Test(expected = AircraftException.class)
	public void constructorDepartureTimeZeroTest() throws AircraftException {
		airCraft = new A380("A380 flight", 0);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when departure time is less than 0
	 */
	@Test(expected = AircraftException.class)
	public void constructorDepartureTimeNegativeTest() throws AircraftException {
		airCraft = new A380("A380 flight", -1);
	}
	
	
	// TESTING CONSTRUCTOR WITH SIX PARAMETERS
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when FlightCode is null
	 */
	@Test(expected = AircraftException.class)
	public void constructorFlightCodeTest() throws AircraftException {
		airCraft = new A380(null, 10, 50, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when Departure Time is 0
	 */
	@Test(expected = AircraftException.class)
	public void constructorZeroDepartureTimeTest() throws AircraftException {
		airCraft = new A380("A380 flight", 0, 50, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when Departure Time is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativeDepartureTimeTest() throws AircraftException {
		airCraft = new A380("A380 flight", -1, 50, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * First (capacity) can be zero. Does not throw an exception. 
	 */
	@Test
	public void constructorZeroFirstTest() throws AircraftException {
		airCraft = new A380("A380 flight", 1, 0, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when First (capacity) is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativeFirstTest() throws AircraftException {
		airCraft = new A380("A380 flight", 1, -1, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Business (capacity) can be zero. Does not throw an exception. 
	 */
	@Test
	public void constructorZeroBusinesstTest() throws AircraftException {
		airCraft = new A380("A380 flight", 1, 50, 0, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when Business (capacity) is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativeBusinessTest() throws AircraftException {
		airCraft = new A380("A380 flight", 1, 50, -1, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Premium (capacity) can be zero. Does not throw an exception. 
	 */
	@Test
	public void constructorZeroPremiumTest() throws AircraftException {
		airCraft = new A380("A380 flight", 1, 50, 50, 0, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when Premium (capacity) is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativePremiumTest() throws AircraftException {
		airCraft = new A380("A380 flight", 1, 50, 50, -1, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Economy (capacity) can be zero. Does not throw an exception. 
	 */
	@Test
	public void constructorZeroEconomyTest() throws AircraftException {
		airCraft = new A380("A380 flight", 1, 50, 50, 0, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when Economy (capacity) is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativeEconomyTest() throws AircraftException {
		
		airCraft = new A380("A380 flight", 1, 50, 50, -1, 50);
	}
	
	
	// TESTING FUNCTIONS IN AICRAFT
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#seatsAvailable(asgn2Passengers.Passenger)}.
	 * @throws AircraftException 
	 * Tests that there are seats available in Economy class
	 */
	@Test
	public void seatsAvailableEmptyFareClassTest() throws AircraftException {
		Boolean available = airCraft.seatsAvailable(pE); 
		int numEconomy = airCraft.getNumEconomy();
		// Economy class is empty
		assertEquals(0, numEconomy);
		assertEquals(true, available);
		
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#seatsAvailable(asgn2Passengers.Passenger)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests that there are no seats available in Economy class
	 */
	@Test
	public void seatsAvailableFullFareClassTest() throws PassengerException, AircraftException {
		createFullEconomy();
		
		Boolean availableNot = airCraft.seatsAvailable(pE);
		int numEconomyFull = airCraft.getNumEconomy();
		assertEquals(371, numEconomyFull);
		assertEquals(false, availableNot);
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if throws an AircraftException if tries to confirm more passengers in first class greater than 14
	 */ 
	@Test(expected = AircraftException.class)
	public void confirmBookingAircraftExceptionFirstClassTest() throws AircraftException, PassengerException {
		//Array containing first class passengers
		ArrayList<First> first = new ArrayList<First>();
		
		//Creating 15 first class passengers. 
		//14 is the limit of passengers for First class
		for (int i = 0; i<15; i++){
			first.add( new First(10, 20) ); //paremeters: bookingTime, DepartureTime
		}
		
		// This loop tries to confirm bookings of 15 passengers
		// It should throw an AircraftException in the last loop
		for (First f1: first){
			airCraft.confirmBooking(f1, 15);//parameters: passenger, ConfirmationTime
		}
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException
	 * @throws PassengerException  
	 * Tests if throws an AircraftException if tries to confirm more passengers in Business class greater than 64
	 */ 
	@Test(expected = AircraftException.class)
	public void confirmBookingAircraftExceptionBusinessClassTest() throws AircraftException, PassengerException {
		//Array containing business class passengers
		ArrayList<Business> business = new ArrayList<Business>();
		
		//Creating 65 business class passengers. 
		//64 is the limit of passengers for business class
		for (int i = 0; i<65; i++){
			business.add( new Business(10, 20) ); //paremeters: bookingTime, DepartureTime
		}
		
		// This loop tries to confirm bookings of 65 passengers
		// It should throw an AircraftException in the last loop		
		for (Business b1: business){
			airCraft.confirmBooking(b1, 15);//parameters: passenger, ConfirmationTime
		}
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if throws an AircraftException if tries to confirm more passengers in Premium class greater than 35
	 */ 
	@Test(expected = AircraftException.class)
	public void confirmBookingAircraftExceptionPremiumClassTest() throws AircraftException, PassengerException {
		//Array containing premium class passengers
		ArrayList<Premium> premium = new ArrayList<Premium>();
		
		//Creating 36 premium class passengers. 
		//35 is the limit of passengers for Premium class
		for (int i = 0; i<36; i++){
			premium.add( new Premium(10, 20) ); //paremeters: bookingTime, DepartureTime
		}
		
		// This loop tries to confirm bookings of 36 passengers
		// It should throw an AircraftException in the last loop
		for (Premium p1: premium){
			airCraft.confirmBooking(p1, 15);//parameters: passenger, ConfirmationTime
		}
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException
	 * @throws PassengerException  
	 * Tests if throws an AircraftException if tries to confirm more passengers in Economy class greater than 35
	 */ 
	@Test(expected = AircraftException.class)
	public void confirmBookingAircraftExceptionEconomyClassTest() throws AircraftException, PassengerException {
		//Array containing economy class passengers
		ArrayList<Economy> economy = new ArrayList<Economy>();
		
		//Creating 372 economy class passengers. 
		//371 is the limit of passengers for Economy class
		for (int i = 0; i<372; i++){
			economy.add( new Economy(10, 20) ); //paremeters: bookingTime, DepartureTime
		}
		
		// This loop tries to confirm bookings of 372 passengers
		// It should throw an AircraftException in the last loop
		for (Economy e1: economy){
			airCraft.confirmBooking(e1, 15);//parameters: passenger, ConfirmationTime
		}

	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests confirmBookings function by checking if numFirst increases accordingly.
	 */ 
	@Test
	public void confirmBookingIncreasesNumfirstTest() throws AircraftException, PassengerException {
		//Creates and confirms 14 first class passengers
		createFullFirst();
		
		//Getting number of passengers in first class
		int num = airCraft.getNumFirst();
		assertEquals(14, num);
		
	}

	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException
	 * @throws PassengerException  
	 * Tests confirmBookings function by checking if numBusiness increases accordingly.
	 */ 
	@Test
	public void confirmBookingIncreasesNumBusinessTest() throws AircraftException, PassengerException {
		//Creates and confirms 64 business class passengers
		createFullBusiness();
		
		//Getting number of passengers in business class
		int num = airCraft.getNumBusiness();
		assertEquals(64, num);
		
	}

	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests confirmBookings function by checking if numPremium increases accordingly.
	 */ 
	@Test
	public void confirmBookingIncreasesNumPremiumTest() throws AircraftException, PassengerException {
		//Creates and confirms 35 premium class passengers
		createFullPremium();
		
		//Getting number of passengers in premium class
		int num = airCraft.getNumPremium();
		assertEquals(35, num);
		
	}


	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests confirmBookings function by checking if numEconomy increases accordingly.
	 */ 
	@Test
	public void confirmBookingIncreasesNumEconomyTest() throws AircraftException, PassengerException {
		//Creates and confirms 371 economy class passengers
		createFullEconomy();
		
		//Getting number of passengers in premium class
		int num = airCraft.getNumEconomy();
		assertEquals(371, num);
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#hasPassenger(asgn2Passengers.Passenger)}.
	 * @throws AircraftException 
	 * @throws PassengerException  	 
	 * Test if hasPassenger returns true after confirming at least one booking
	 */
	@Test
	public void hasPassengerTrueTest() throws AircraftException, PassengerException {
		//A passenger is added to the aircraft and confirmed once booking is confirmed
		airCraft.confirmBooking(pE, 15);
		
		boolean has = airCraft.hasPassenger(pE);
		assertEquals(true, has);
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#hasPassenger(asgn2Passengers.Passenger)}.
	 * @throws AircraftException 
	 * @throws PassengerException  	 
	 * Tests if hasPassenger() function returns false if there are no confirmed passengers 
	 */
	@Test
	public void hasPassengerFalseTest() throws AircraftException, PassengerException {
		boolean has = airCraft.hasPassenger(pE);
		assertEquals(false, has);
		
	}

	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if throws an AircraftException when trying to cancel the booking
	 * of a passenger that is not in the aircraft.
	 */
	@Test(expected = AircraftException.class)
	public void cancelBookingAircraftExceptionTest() throws PassengerException, AircraftException {
		// Aircraft does not have the passenger pE
		boolean has = airCraft.hasPassenger(pE);
		assertEquals(false, has);
		
		// Next statement should throw an exception
		airCraft.cancelBooking(pE, 12);
		
	}
		
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if aircraft still has the passenger after canceling its booking
	 */
	@Test
	public void cancelBookingAfterConfirmBookingTest() throws PassengerException, AircraftException {
		airCraft.confirmBooking(pF, 15);
		boolean has = airCraft.hasPassenger(pF);
		assertEquals(true, has);
		
		airCraft.cancelBooking(pF, 12);
		boolean hasNot = airCraft.hasPassenger(pF);
		assertEquals(false, hasNot);
		
	}


	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumPassengers()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if getNumPassengers returns the total number of passengers in the aircraft 
	 */
	@Test
	public void getNumPassengersTotalTest() throws AircraftException, PassengerException {
		//Confirming one passenger of each class to aircraft
		airCraft.confirmBooking(pE, 12);
		airCraft.confirmBooking(pP, 12);
		airCraft.confirmBooking(pB, 12);
		airCraft.confirmBooking(pF, 12);
		
		int totalNumber = airCraft.getNumPassengers();
		assertEquals(4, totalNumber);
		
	}
		
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightEmpty()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if FlightEmpty() returns false when there are passengers in the aircraft. 
	 */
	@Test
	public void flightEmptyNotEmptyTest() throws AircraftException, PassengerException {
		//Confirming one passenger of each class to aircraft
		airCraft.confirmBooking(pE, 12);
		airCraft.confirmBooking(pP, 12);
		airCraft.confirmBooking(pB, 12);
		airCraft.confirmBooking(pF, 12);
		
		int totalNumber = airCraft.getNumPassengers();
		assertEquals(4, totalNumber);
		
		boolean notEmpty = airCraft.flightEmpty();
		assertEquals(false, notEmpty);
		
	}

	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightEmpty()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if FlightEmpty() returns true when aircraft is empty of passengers. 
	 */
	@Test
	public void flightEmptyTest() throws AircraftException, PassengerException {
		// No bookings have been confirmed before calling the next function		
		boolean empty = airCraft.flightEmpty();
		assertEquals(true, empty);
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightFull()}.
	 * Tests if FlightFull() returns false if there are empty seats on the airceaft 
	 */
	@Test
	public void flightFullNoPassengersTest(){
		//There are no passengers confirmed in this aircraft
		boolean notFull = airCraft.flightFull();
		assertEquals(false, notFull);
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightFull()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if FlightFull() returns true if every seat on aircraft is full
	 */
	@Test
	public void flightFullIsFullTest() throws PassengerException, AircraftException{
		//Creating passengers and filling aircraft with confirmed passengers
		createFullEconomy();
		createFullPremium();
		createFullBusiness();
		createFullFirst();
		
		boolean full = airCraft.flightFull();
		assertEquals(true, full);
		
	}


	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getPassengers()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if getPassengers() contains all passengers of the aircraft
	 */
	@Test
	public void getPassengersTest() throws PassengerException, AircraftException {
		//Creating passengers and filling aircraft with confirmed passengers
		createFullEconomy();
		createFullPremium();
		createFullBusiness();
		createFullFirst();
		
		ArrayList<Passenger> passList = (ArrayList<Passenger>) airCraft.getPassengers();
		
		int numberPassengers = 0;
		for(Passenger pass: passList){
			numberPassengers ++;
		}
		
		int totalCapacity = airCraft.getNumEconomy() + airCraft.getNumPremium() +
				airCraft.getNumBusiness() + airCraft.getNumFirst();
		
		assertEquals(totalCapacity, numberPassengers);
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flyPassengers(int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if flyPassenger() function changes the state of all confirmed passengers to flown
	 */
	@Test
	public void flyPassengersAllConfirmedPassengersTest() throws PassengerException, AircraftException {
		//Creating passengers and filling aircraft with confirmed passengers
		createFullEconomy();
		createFullPremium();
		createFullBusiness();
		createFullFirst();
		
		airCraft.flyPassengers(40);
		
		ArrayList<Passenger> passList = (ArrayList<Passenger>) airCraft.getPassengers();
		boolean areFlown = true;
		for(Passenger pass: passList){
			areFlown = areFlown && pass.isFlown();
		}
		
		assertEquals(true, areFlown);
		
	}

	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flyPassengers(int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests checks if only the Economy passengers have been flown
	 */
	@Test
	public void flyPassengersOnlyConfirmedPassengersTest() throws PassengerException, AircraftException {
		//Creating passengers and filling Economy seats with confirmed passengers
		createFullEconomy();

		airCraft.flyPassengers(40);
		
		//Creating passengers and filling Business seats with confirmed passengers
		createFullBusiness();
		
		ArrayList<Passenger> passList = (ArrayList<Passenger>) airCraft.getPassengers();
		
		boolean areFlown = true;
		for(Passenger pass: passList){
			areFlown = areFlown && pass.isFlown();
		}
		
		assertEquals(false, areFlown);
		
	}


	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if Business passengers are upgraded to First class
	 */
	@Test
	public void upgradeBookingsFromBusinessToFirst() throws PassengerException, AircraftException {
		//create 64 Business class passengers
		createFullBusiness();
		
		int numBusinessBeforeUpgrade = airCraft.getNumBusiness();
		
		airCraft.upgradeBookings();
		
		int numFirst = airCraft.getNumFirst();
		int numBusinessAfterUpgrade = airCraft.getNumBusiness();
		
		assertEquals(14, numFirst);
		assertEquals(50,numBusinessAfterUpgrade);
		assertEquals(64,numBusinessBeforeUpgrade);
		assertEquals(numBusinessBeforeUpgrade, numFirst + numBusinessAfterUpgrade );
		
	}

	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if every passenger is upgraded into the next class until this class is full
	 */
	@Test
	public void upgradeBookingsFromAllClassesTest() throws PassengerException, AircraftException {
		//Filling every fare class except First
		createFullBusiness();
		createFullPremium();
		createFullEconomy();
		
		int numFirstBeforeUpgrade = airCraft.getNumFirst();
		int numBusinessBeforeUpgrade = airCraft.getNumBusiness();
		int numPremiumBeforeUpgrade = airCraft.getNumPremium();
		int numEconomyBeforeUpgrade = airCraft.getNumEconomy();
		
		airCraft.upgradeBookings();
		
		int numFirstAfterUpgrade = airCraft.getNumFirst();
		int numBusinessAfterUpgrade = airCraft.getNumBusiness();
		int numPremiumAfterUpgrade = airCraft.getNumPremium();
		int numEconomyAfterUpgrade = airCraft.getNumEconomy();
		
		assertEquals(14, numFirstAfterUpgrade);
		assertEquals(64,numBusinessAfterUpgrade);
		assertEquals(35,numPremiumAfterUpgrade);
		assertEquals(357,numEconomyAfterUpgrade);
		
		int totalNumberBefore = numFirstBeforeUpgrade + numBusinessBeforeUpgrade + numPremiumBeforeUpgrade + numEconomyBeforeUpgrade;
		int totalNumberAfter = numFirstAfterUpgrade + numBusinessAfterUpgrade + numPremiumAfterUpgrade + numEconomyAfterUpgrade;
		
		assertEquals(totalNumberAfter, totalNumberBefore );
		
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 * Tests if Bookings object has the data passed from Aircraft getBooking() function. 
	 */
	@Test
	public void getBookingsTest() throws PassengerException, AircraftException{
		createFullFirst();
		createFullBusiness();
		createFullPremium();
		createFullEconomy();
		int totalPassengersAircraft = airCraft.getNumEconomy() + airCraft.getNumPremium() + 
				airCraft.getNumBusiness() + airCraft.getNumFirst();
		
		Bookings bookings = airCraft.getBookings();
		int bookNumEconomy = bookings.getNumEconomy();
		int bookNumPremium = bookings.getNumPremium();
		int bookNumBusiness = bookings.getNumBusiness();
		int bookNumFirst = bookings.getNumFirst();
		int bookTotalPassengers = bookings.getTotal();
		int bookingAvailableSeats = bookings.getAvailable();
		
		assertEquals(airCraft.getNumEconomy(), bookNumEconomy);
		assertEquals(airCraft.getNumPremium(), bookNumPremium);
		assertEquals(airCraft.getNumBusiness(), bookNumBusiness);
		assertEquals(airCraft.getNumFirst(), bookNumFirst);
		assertEquals(totalPassengersAircraft, bookTotalPassengers);
		assertEquals(0, bookingAvailableSeats);
		
	}
	
	
	/**
	 * Helper Method to create First class Passengers to the limit
	 */
	private void createFullFirst() throws PassengerException, AircraftException {
		//Array containing first class passengers
		ArrayList<First> first = new ArrayList<First>();
		
		//Creating 14 first class passengers. 
		for (int i = 0; i<14; i++){
			first.add( new First(10, 20) ); //paremeters: bookingTime, DepartureTime
		}
		
		// This loop confirms bookings of 14 first class passengers
		for (First f1: first){
			airCraft.confirmBooking(f1, 15);//parameters: passenger, ConfirmationTime
		}
		
	}
	
	
	/**
	 * Helper Method to create Business class Passengers to the limit
	 */
	private void createFullBusiness() throws PassengerException, AircraftException {
		//Array containing first class passengers
		ArrayList<Business> business = new ArrayList<Business>();
		
		//Creating 64 Business class passengers. 
		for (int i = 0; i<64; i++){
			business.add( new Business(10, 20) ); //paremeters: bookingTime, DepartureTime
		}
		
		// This loop confirms bookings of 64 business class passengers
		for (Business b1: business){
			airCraft.confirmBooking(b1, 15);//parameters: passenger, ConfirmationTime
		}
		
	}
	
	
	/**
	 * Helper Method to create Premium class Passengers to the limit
	 */
	private void createFullPremium() throws PassengerException, AircraftException {
		//Array containing premium class passengers
		ArrayList<Premium> premium = new ArrayList<Premium>();
		
		//Creating 35 Premium class passengers. 
		for (int i = 0; i<35; i++){
			premium.add( new Premium(10, 20) ); //paremeters: bookingTime, DepartureTime
		}
		
		// This loop confirms bookings of 35 premium class passengers
		for (Premium p1: premium){
			airCraft.confirmBooking(p1, 15);//parameters: passenger, ConfirmationTime
		}
		
	}
	
	
	/**
	 * Helper Method to create Economy class Passengers to the limit
	 */
	private void createFullEconomy() throws PassengerException, AircraftException {
		//Array containing economy class passengers
		ArrayList<Economy> economy = new ArrayList<Economy>();
		
		//Creating 371 Economy class passengers. 
		for (int i = 0; i<371; i++){
			economy.add( new Economy(10, 20) ); //paremeters: bookingTime, DepartureTime
		}
		
		// This loop confirms bookings of 371 economy class passengers
		for (Economy e1: economy){
			airCraft.confirmBooking(e1, 15);//parameters: passenger, ConfirmationTime
		}
		
	}
	
	
}
