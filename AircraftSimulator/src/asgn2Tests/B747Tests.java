package asgn2Tests;
import org.junit.Before;
import org.junit.Test;
import asgn2Aircraft.AircraftException;
import asgn2Aircraft.B747;
import asgn2Passengers.PassengerException;

/**
 * @author Renzo & James
 *
 */
public class B747Tests {

	private B747 airCraft;
		
		
	/**
	 * Set Up method to implement tests
	 */
	@Before
	public void noTestSetUp() throws Exception {
		airCraft = new B747("B747", 20);
	}
	
	
	// TESTING CONSTRUCTOR WITH TWO PARAMETERS
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int)}.
	 * @throws AircraftException 
	 * Empty string is allowed. Does not throw an exception
	 */
	public void constructorStringTest() throws AircraftException {
		airCraft = new B747("", 1);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AicraftException when FlightCode is null
	 */
	@Test(expected = AircraftException.class)
	public void constructorStringNullTest() throws AircraftException {
		airCraft = new B747(null, 1);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when departure time is 0
	 */
	@Test(expected = AircraftException.class)
	public void constructorDepartureTimeZeroTest() throws AircraftException {
		airCraft = new B747("B747 flight", 0);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AicraftException when departure time is less than 0
	 */
	@Test(expected = AircraftException.class)
	public void constructorDepartureTimeNegativeTest() throws AircraftException {
		airCraft = new B747("B747 flight", -1);
	}
	
	
	// TESTING CONSTRUCTOR WITH SIX PARAMETERS
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AicraftException when FlightCode is null
	 */
	@Test(expected = AircraftException.class)
	public void constructorFlightCodeTest() throws AircraftException {
		airCraft = new B747(null, 10, 50, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftExeption when Departure Time is 0
	 */
	@Test(expected = AircraftException.class)
	public void constructorZeroDepartureTimeTest() throws AircraftException {
		airCraft = new B747("B747 flight", 0, 50, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when Departure Time is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativeDepartureTimeTest() throws AircraftException {
		airCraft = new B747("B747 flight", -1, 50, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * First (capacity) can be zero. Does not throw an exception. 
	 */
	@Test
	public void constructorZeroFirstTest() throws AircraftException {
		airCraft = new B747("B747 flight", 1, 0, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AricraftException when First (capacity) is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativeFirstTest() throws AircraftException {
		airCraft = new B747("B747 flight", 1, -1, 50, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Business (capacity) can be zero. Does not throw an exception. 
	 */
	@Test
	public void constructorZeroBusinesstTest() throws AircraftException {
		airCraft = new B747("B747 flight", 1, 50, 0, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when Business (capacity) is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativeBusinessTest() throws AircraftException {
		airCraft = new B747("B747 flight", 1, 50, -1, 50, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Premium (capacity) can be zero. Does not throw an exception. 
	 */
	@Test
	public void constructorZeroPremiumTest() throws AircraftException {
		airCraft = new B747("B747 flight", 1, 50, 50, 0, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when Premium (capacity) is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativePremiumTest() throws AircraftException {
		airCraft = new B747("B747 flight", 1, 50, 50, -1, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException
	 * Economy (capacity) can be zero. Does not throw an exception. 
	 */
	@Test
	public void constructorZeroEconomyTest() throws AircraftException {
		airCraft = new B747("B747 flight", 1, 50, 50, 0, 50);
	}
	
	
	/**
	 * Test method for {@link asgn2Aircraft.B747#B747(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 * Tests if throws an AircraftException when Economy (capacity) is negative
	 */
	@Test(expected = AircraftException.class)
	public void constructorNegativeEconomyTest() throws AircraftException {
		airCraft = new B747("B747 flight", 1, 50, 50, -1, 50);
	}

}
