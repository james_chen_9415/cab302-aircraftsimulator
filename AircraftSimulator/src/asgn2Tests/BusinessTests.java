/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

/**
 * @author Renzo & James
 *
 */
public class BusinessTests {

	/**
	 * Test method for {@link asgn2Passengers.Business#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testUpgrade() throws PassengerException {
		Business businessPassenger = new Business(0, 1);
		businessPassenger.confirmSeat(1, 2);
		// First upgrade1 = (First) businessPassenger.upgrade();

		Passenger upgrade = businessPassenger.upgrade();

		// assertTrue(upgrade instanceof First);
		// assertFalse(upgrade instanceof Business);

		assertEquals(businessPassenger.isNew(), upgrade.isNew());
		assertEquals(businessPassenger.isConfirmed(), upgrade.isConfirmed());
		assertEquals(businessPassenger.isFlown(), upgrade.isFlown());
		assertEquals(businessPassenger.isQueued(), upgrade.isQueued());
		assertEquals(businessPassenger.isRefused(), upgrade.isRefused());
		assertEquals(businessPassenger.getBookingTime(), upgrade.getBookingTime());
		assertEquals(businessPassenger.getDepartureTime(), upgrade.getDepartureTime());
		assertEquals(businessPassenger.getEnterQueueTime(), upgrade.getEnterQueueTime());
		assertEquals(businessPassenger.getExitQueueTime(), upgrade.getExitQueueTime());
		assertEquals(businessPassenger.getConfirmationTime(), upgrade.getConfirmationTime());

	}

}
