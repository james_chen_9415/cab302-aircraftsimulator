/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Passengers.Economy;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

/**
 * @author Renzo & James
 *
 */
public class EconomyTests {

	/**
	 * Test method for {@link asgn2Passengers.Economy#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testUpgrade() throws PassengerException {
		Economy economyPassenger = new Economy(0, 1);
		economyPassenger.confirmSeat(1, 2);
		Premium upgrade = (Premium) economyPassenger.upgrade();

		assertEquals(economyPassenger.isNew(), upgrade.isNew());
		assertEquals(economyPassenger.isConfirmed(), upgrade.isConfirmed());
		assertEquals(economyPassenger.isFlown(), upgrade.isFlown());
		assertEquals(economyPassenger.isQueued(), upgrade.isQueued());
		assertEquals(economyPassenger.isRefused(), upgrade.isRefused());
		assertEquals(economyPassenger.getBookingTime(), upgrade.getBookingTime());
		assertEquals(economyPassenger.getDepartureTime(), upgrade.getDepartureTime());
		assertEquals(economyPassenger.getEnterQueueTime(), upgrade.getEnterQueueTime());
		assertEquals(economyPassenger.getExitQueueTime(), upgrade.getExitQueueTime());
		assertEquals(economyPassenger.getConfirmationTime(), upgrade.getConfirmationTime());

	}

}
