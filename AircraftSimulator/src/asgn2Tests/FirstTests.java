/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.First;
import asgn2Passengers.PassengerException;

/**
 * @author Renzo & James
 *
 */
public class FirstTests {

	First firstPassenger;

	
	/**
	 * @throws PassengerException
	 */
	@Before
	public void setup() throws PassengerException {
		firstPassenger = new First(0, 1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.First#First()}.
	 *
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFirstWithInvalidBookingTime() throws PassengerException {
		int bookingTime = -1;
		int departureTime = 1;
		firstPassenger = new First(bookingTime, departureTime);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.First#First()}.
	 *
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFirstWithInvalidDepartureTime() throws PassengerException {
		int bookingTime = 0;
		int departureTime = 0;
		firstPassenger = new First(bookingTime, departureTime);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.First#First()}.
	 *
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFirstWithInvalidBothOfTimes() throws PassengerException {
		int bookingTime = -1;
		int departureTime = 0;
		firstPassenger = new First(bookingTime, departureTime);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.First#First()}.
	 *
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFirstWithDepartureTimeSmallerThanBookingTime() throws PassengerException {
		int bookingTime = 2;
		int departureTime = 1;
		firstPassenger = new First(bookingTime, departureTime);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testConfirmSeatAlreadyConfirmed() throws PassengerException {
		firstPassenger.confirmSeat(0, 1);
		firstPassenger.confirmSeat(0, 1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testConfirmSeatAlreadyRefused() throws PassengerException {
		firstPassenger.refusePassenger(0);
		firstPassenger.confirmSeat(0, 1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testConfirmSeatAlreadyFlown() throws PassengerException {
		firstPassenger.flyPassenger(0);
		firstPassenger.confirmSeat(0, 1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testConfirmSeatInvalidConfirmationTime() throws PassengerException {
		firstPassenger.confirmSeat(-1, 1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testConfirmSeatDepartureTimeSmallerThanConfirmationTime() throws PassengerException {
		firstPassenger.confirmSeat(1, 0);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testConfirmSeatDepartureCorrectValue() throws PassengerException {
		firstPassenger.confirmSeat(0, 1);

		// Test more times
		for (int confirmationTime = 0; confirmationTime < 5; confirmationTime++) {
			setup();
			firstPassenger.confirmSeat(confirmationTime, confirmationTime);
			setup();
			firstPassenger.confirmSeat(confirmationTime, confirmationTime + 1);
		}
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFlyPassengerNewState() throws PassengerException {
		firstPassenger.flyPassenger(1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFlyPassengerInQueue() throws PassengerException {
		firstPassenger.queuePassenger(2, 3);
		firstPassenger.flyPassenger(1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFlyPassengerRefuseState() throws PassengerException {
		firstPassenger.refusePassenger(2);
		firstPassenger.flyPassenger(1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFlyPassengerFlownState() throws PassengerException {
		firstPassenger.flyPassenger(1);
		firstPassenger.flyPassenger(2);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFlyPassengerInvalidDepartureTime() throws PassengerException {
		firstPassenger.flyPassenger(0);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testFlyPassengerInvalidDepartureTimeNegative() throws PassengerException {
		firstPassenger.flyPassenger(-1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testFlyPassengerCorrect() throws PassengerException {
		firstPassenger.confirmSeat(1, 2);
		firstPassenger.flyPassenger(3);
	}

	
	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testQueuePassengerAlreadyConfirmed() throws PassengerException {
		firstPassenger.confirmSeat(1, 2);
		firstPassenger.queuePassenger(2, 3);
	}

	
	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testQueuePassengerAlreadyRefused() throws PassengerException {
		firstPassenger.refusePassenger(0);
		firstPassenger.queuePassenger(2, 3);
	}

	
	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testQueuePassengerAlreadyQueue() throws PassengerException {
		firstPassenger.queuePassenger(2, 3);
		firstPassenger.queuePassenger(2, 3);
	}

	
	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testQueuePassengerAlreadyFlown() throws PassengerException {
		firstPassenger.flyPassenger(0);
		firstPassenger.queuePassenger(2, 3);
	}

	
	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testQueuePassengerWithInvalidQueueTime() throws PassengerException {
		firstPassenger.queuePassenger(-1, 3);
	}

	
	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testQueuePassengerDepartureTimeSmallerThanQueueTime() throws PassengerException {
		firstPassenger.queuePassenger(3, 2);
	}

	
	/**
	 * Test method for
	 * {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testQueuePassengerCorrectValue() throws PassengerException {
		firstPassenger.queuePassenger(1, 1);
		assertEquals(true, firstPassenger.isQueued());
		assertEquals(false, firstPassenger.isNew());
		assertEquals(1, firstPassenger.getEnterQueueTime());
		assertEquals(1, firstPassenger.getDepartureTime());
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testRefusePassengerAlreadyConfirmed() throws PassengerException {
		firstPassenger.confirmSeat(1, 2);
		firstPassenger.refusePassenger(2);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testRefusePassengerAlreadyRefused() throws PassengerException {
		firstPassenger.refusePassenger(2);
		firstPassenger.refusePassenger(2);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testRefusePassengerAlreadyFlown() throws PassengerException {
		firstPassenger.flyPassenger(3);
		firstPassenger.refusePassenger(2);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testRefusePassengerInvalidRefusalTime() throws PassengerException {
		firstPassenger.refusePassenger(-1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testRefusePassengerRefusalTimeSmallerThanBookingTime() throws PassengerException {
		firstPassenger = new First(1, 2);
		firstPassenger.refusePassenger(0);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testRefusePassengerFromNewState() throws PassengerException {
		firstPassenger.refusePassenger(0);
		assertEquals(true, firstPassenger.isRefused());
		assertEquals(false, firstPassenger.isNew());
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testRefusePassengerFromQueueState() throws PassengerException {
		firstPassenger.queuePassenger(2, 3);
		firstPassenger.refusePassenger(4);
		assertEquals(true, firstPassenger.isRefused());
		assertEquals(false, firstPassenger.isNew());
		assertEquals(false, firstPassenger.isQueued());
		assertEquals(4, firstPassenger.getExitQueueTime());
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testCancelSeatAlreadyNewState() throws PassengerException {
		firstPassenger.cancelSeat(0);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testCancelSeatAlreadyQueue() throws PassengerException {
		firstPassenger.queuePassenger(1, 2);
		firstPassenger.cancelSeat(2);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testCancelSeatAlreadyRefuse() throws PassengerException {
		firstPassenger.refusePassenger(2);
		firstPassenger.cancelSeat(1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testCancelSeatInvalidCancellationTime() throws PassengerException {
		firstPassenger.confirmSeat(2, 3);
		firstPassenger.cancelSeat(-1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test(expected = PassengerException.class)
	public final void testCancelSeatDepartureTimeSmallerThanCancellationTime() throws PassengerException {
		firstPassenger.confirmSeat(2, 3);
		firstPassenger.cancelSeat(4);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testCancelSeatCorrectCancellation() throws PassengerException {
		firstPassenger.confirmSeat(2, 3);
		firstPassenger.cancelSeat(2);
		assertEquals(true, firstPassenger.isNew());
		assertEquals(false, firstPassenger.isConfirmed());
		assertEquals(2, firstPassenger.getBookingTime());
	}

	
	/**
	 * Test method for {@link asgn2Passengers.First#upgrade()}.
	 */
	@Test
	public final void testUpgrade() {
		assertTrue(firstPassenger.upgrade() instanceof First);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#wasConfirmed()} .
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testWasConfirmed() throws PassengerException {
		firstPassenger.confirmSeat(2, 3);
		firstPassenger.cancelSeat(2);
		assertEquals(true, firstPassenger.wasConfirmed());
	}

	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#WasQueued()} .
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testWasQueued() throws PassengerException {
		firstPassenger.queuePassenger(1, 2);
		firstPassenger.confirmSeat(2, 3);
		firstPassenger.cancelSeat(2);
		assertEquals(true, firstPassenger.wasQueued());
	}

}
