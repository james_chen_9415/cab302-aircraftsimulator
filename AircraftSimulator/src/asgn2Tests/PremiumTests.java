/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

/**
 * @author Renzo & James
 *
 */
public class PremiumTests {

	/**
	 * Test method for {@link asgn2Passengers.Premium#upgrade()}.
	 * 
	 * @throws PassengerException
	 */
	@Test
	public final void testUpgrade() throws PassengerException {
		Premium premiumPassenger = new Premium(0, 1);
		premiumPassenger.confirmSeat(1, 2);
		Business upgrade = (Business) premiumPassenger.upgrade();

		assertEquals(premiumPassenger.isNew(), upgrade.isNew());
		assertEquals(premiumPassenger.isConfirmed(), upgrade.isConfirmed());
		assertEquals(premiumPassenger.isFlown(), upgrade.isFlown());
		assertEquals(premiumPassenger.isQueued(), upgrade.isQueued());
		assertEquals(premiumPassenger.isRefused(), upgrade.isRefused());
		assertEquals(premiumPassenger.getBookingTime(), upgrade.getBookingTime());
		assertEquals(premiumPassenger.getDepartureTime(), upgrade.getDepartureTime());
		assertEquals(premiumPassenger.getEnterQueueTime(), upgrade.getEnterQueueTime());
		assertEquals(premiumPassenger.getExitQueueTime(), upgrade.getExitQueueTime());
		assertEquals(premiumPassenger.getConfirmationTime(), upgrade.getConfirmationTime());

	}

}
